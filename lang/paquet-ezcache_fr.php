<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/cache.git

return [

	// E
	'ezcache_description' => 'Framework de gestion des caches pour plugins. Ce plugin fournit les API et les mécanismes permettant d’écrire, de lire et de supprimer des caches spécifiques à un plugin utilisateur.',
	'ezcache_slogan' => 'La fabrique de caches pour plugins',
];
