<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cache-cache?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// C
	'cache_liste_label_fichier' => 'Nome',
	'cache_liste_label_type' => 'Tipo de cache',
	'cache_vider_cocher_rien' => 'Desmarcar tudo',
	'cache_vider_cocher_tout' => 'Marcar tudo',
	'cache_vider_erreur_nocache' => 'É obrigatório marcar pelo menos um cache.',
	'cache_vider_fieldset_form' => 'Tipo de cache «@type@»',
	'cache_vider_label_choix' => 'Escolher os caches a excluir',
	'cache_vider_menu' => 'Limpar os caches dos plugins',
	'cache_vider_notice_0_cache' => 'O plugin ainda não criou nenhum cache.',
	'cache_vider_notice_0_plugin' => 'Nenhum plugin ativo está usando caches gerenciáveis pelo Cache Factory.',
	'cache_vider_succes' => 'Os caches selecionados foram devidamente excluídos.',
	'cache_vider_titre_form' => 'Caches do plugin @plugin@',
	'cache_vider_titre_page' => 'Cache Factory',

	// I
	'info_1_cache' => 'Um cache',
	'info_aucun_cache' => 'Nenhum cache',
	'info_nb_caches' => '@nb@ caches',
];
