<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/cache.git

return [

	// C
	'cache_liste_label_fichier' => 'Nom',
	'cache_liste_label_type' => 'Type de cache',
	'cache_vider_cocher_rien' => 'Tout décocher',
	'cache_vider_cocher_tout' => 'Tout cocher',
	'cache_vider_erreur_nocache' => 'Il est obligatoire de sélectionner au moins un cache.',
	'cache_vider_fieldset_form' => 'Type de cache « @type@ »',
	'cache_vider_label_choix' => 'Choisir les caches à supprimer',
	'cache_vider_menu' => 'Vider les caches des plugins',
	'cache_vider_notice_0_cache' => 'Aucun cache n’a encore été créé par le plugin.',
	'cache_vider_notice_0_plugin' => 'Aucun plugin actif n’utilise des caches administrables de Cache Factory.',
	'cache_vider_succes' => 'Les caches sélectionnés ont bien été supprimés.',
	'cache_vider_titre_form' => 'Caches du plugin @plugin@',
	'cache_vider_titre_page' => 'Cache Factory',

	// I
	'info_1_cache' => 'Un cache',
	'info_aucun_cache' => 'Aucun cache',
	'info_nb_caches' => '@nb@ caches',
];
