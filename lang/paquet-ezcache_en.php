<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/ezcache-paquet-xml-cache?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'ezcache_description' => 'Cache management framework for plugins. This plugin provides APIs and mechanisms for writing, reading and deleting plugin-specific caches.',
	'ezcache_slogan' => 'Cache factory for plugins',
];
