<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cache-cache?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'cache_liste_label_fichier' => 'Filename',
	'cache_liste_label_type' => 'Cache type',
	'cache_vider_cocher_rien' => 'Uncheck all',
	'cache_vider_cocher_tout' => 'Check all',
	'cache_vider_erreur_nocache' => 'At least one cache must be selected.',
	'cache_vider_fieldset_form' => '"@type@" cache type',
	'cache_vider_label_choix' => 'Select caches to delete',
	'cache_vider_menu' => 'Empty plugin caches',
	'cache_vider_notice_0_cache' => 'No cache has yet been created by the plugin.',
	'cache_vider_notice_0_plugin' => 'No active plugin uses Cache Factory’s manageable caches.',
	'cache_vider_succes' => 'The selected caches have been deleted.',
	'cache_vider_titre_form' => '@plugin@ plugin caches',
	'cache_vider_titre_page' => 'Cache Factory',

	// I
	'info_1_cache' => '1 cache',
	'info_aucun_cache' => 'No cache',
	'info_nb_caches' => '@nb@ caches',
];
