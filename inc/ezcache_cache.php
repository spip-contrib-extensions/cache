<?php
/**
 * Ce fichier contient les fonctions d'API du plugin Cache Factory.
 *
 * @package SPIP\CACHE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ecrit un contenu donné dans un cache spécifié par son identifiant relatif ou par son chemin complet.
 *
 * @api
 *
 * @uses configuration_cache_lire()
 * @uses ezcache_cache_verifier()
 * @uses ezcache_cache_composer()
 * @uses ezcache_cache_decomposer()
 * @uses sous_repertoire()
 * @uses ecrire_fichier()
 * @uses ecrire_fichier_securise()
 *
 * @param string                      $plugin     Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                                                ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param string                      $type_cache Type de cache identifiant la configuration dans la liste des configurations de cache du plugin
 * @param array<string, mixed>|string $cache      Identifiant du cache sous la forme d'une chaine (le chemin du fichier) ou d'un tableau fournissant
 *                                                les composants canoniques du nom.
 * @param array|string                $contenu    Contenu sous forme de tableau à sérialiser/encoder ou sous la forme d'une chaine.
 * @param null|bool                   $post_cache `true` si on doit appeler le pipeline post_cache ou `false` sinon. Le défaut vaut `true`.
 *                                                Permet d'utiliser l'API dans le pipeline post_cache.
 *
 * @return bool True si l'écriture s'est bien passée, false sinon.
 */
function cache_ecrire(string $plugin, string $type_cache, $cache, $contenu, ?bool $post_cache = true) : bool {
	// Initialisation du retour de la fonction
	$cache_ecrit = false;

	// Lecture de la configuration des caches du plugin.
	$configuration = configuration_cache_lire($plugin, $type_cache);

	// Le cache peut-être fourni soit sous la forme d'un chemin complet soit sous la forme d'un
	// tableau permettant de calculer le chemin complet. On prend en compte ces deux cas.
	$fichier_cache = '';
	include_spip('ezcache/ezcache');
	if (
		is_array($cache)
		and ($cache = ezcache_cache_verifier($plugin, $cache, $configuration))
	) {
		// Détermination du chemin du cache :
		// - le nom sans extension est construit à partir des éléments fournis sur le conteneur et
		//   de la configuration du nom pour le plugin.
		$fichier_cache = ezcache_cache_composer($plugin, $cache, $configuration);
	} elseif (is_string($cache)) {
		// Le chemin complet du fichier cache est fourni. Aucune vérification ne peut être faite
		// il faut donc que l'appelant ait utilisé l'API pour calculer le fichier au préalable.
		$fichier_cache = $cache;
		// Dans ce cas, il faut retrouver le cache canonique sous forme tabulaire
		$cache = ezcache_cache_decomposer($plugin, $fichier_cache, $configuration);
	}

	if ($fichier_cache) {
		// On crée les répertoires si besoin
		include_spip('inc/flock');
		$dir_cache = sous_repertoire(
			constant($configuration['racine']),
			rtrim($configuration['dossier_plugin'], '/')
		);
		if ($configuration['sous_dossier']) {
			ezcache_sous_chemin($dir_cache . rtrim($cache['sous_dossier'], '/'));
		}

		// Si le contenu est une chaine alors on ne fait rien même si l'attribut sérialisation ou codage est positionné.
		// Cela permet de passer en douceur de l'attribut decodage à l'atribut codage.
		$contenu_cache = '';
		if (is_string($contenu)) {
			$contenu_cache = $contenu;
		} elseif ($configuration['serialisation']) {
			// TODO : a priori ce test est inutile car si le contenu n'est pas une chaine c'est un tableau (à vérifier)
			if (!is_array($contenu)) {
				$contenu = $contenu ? [$contenu] : [];
			}
			$contenu_cache = serialize($contenu);
		} elseif (
			$configuration['codage']
			and is_array($contenu)
		) {
			// Etant donné qu'en CSV le codage précise le délimiteur et peut donc être différent de l'extension qui
			// permis lors de la configuration de définir le codage par défaut, on autorise le fait de pouvoir le
			// préciser dans le tableau canonique du cache.
			$codage = $cache['codage'] ?? '';
			$contenu_cache = ezcache_cache_encoder($plugin, $codage, $contenu, $configuration);
		}

		// Ecriture du fichier cache sécurisé ou pas suivant la configuration.
		$ecrire = 'ecrire_fichier';
		if ($configuration['securisation']) {
			$ecrire = 'ecrire_fichier_securise';
		}
		$cache_ecrit = $ecrire($fichier_cache, $contenu_cache);

		// Appel d'un pipeline post opération sur le cache (écriture, suppression).
		// Ce pipeline peut être utilisé pour loger le cache dans une liste par exemple.
		if (
			$cache_ecrit
			and $post_cache
		) {
			$flux = [
				'args' => [
					'plugin'        => $plugin,
					'fonction'      => 'ecrire',
					'fichier_cache' => $fichier_cache,
					'cache'         => $cache,
					'configuration' => $configuration
				],
			];
			pipeline('post_cache', $flux);
		}
	}

	return $cache_ecrit;
}

/**
 * Lit le cache spécifié par son identifiant relatif ou son chemin complet et renvoie le contenu sous forme
 * de tableau ou de chaine suivant l’attribut de sérialisation.
 * En cas d’erreur, la fonction renvoie false.
 *
 * @api
 *
 * @uses configuration_cache_lire()
 * @uses ezcache_cache_verifier()
 * @uses ezcache_cache_composer()
 * @uses lire_fichier()
 * @uses lire_fichier_securise()
 * @uses ezcache_cache_decoder()
 *
 * @param string                      $plugin     Identifiant qui permet de distinguer le module appelant qui peut-être
 *                                                un plugin comme le noiZetier ou un script. Pour un plugin, le plus
 *                                                pertinent est d'utiliser le préfixe.
 * @param string                      $type_cache Type de cache identifiant la configuration dans la liste des
 *                                                configurations de cache du plugin
 * @param array<string, mixed>|string $cache      Identifiant du cache sous la forme d'une chaine (le chemin du
 *                                                fichier) ou d'un tableau fournissant les composants canoniques du nom.
 *
 * @return array|bool|string Contenu du fichier sous la forme d'un tableau, d'une chaine ou false si une erreur s'est
 *                           produite.
 * @throws Exception
 */
function cache_lire(string $plugin, string $type_cache, $cache) {
	// Initialisation du contenu du cache
	$cache_lu = false;

	// Lecture de la configuration des caches du plugin.
	$configuration = configuration_cache_lire($plugin, $type_cache);

	// Le cache peut-être fourni soit sous la forme d'un chemin complet soit sous la forme d'un
	// tableau permettant de calculer le chemin complet. On prend en compte ces deux cas.
	$fichier_cache = '';
	include_spip('ezcache/ezcache');
	if (
		is_array($cache)
		and ($cache = ezcache_cache_verifier($plugin, $cache, $configuration))
	) {
		// Détermination du chemin du cache :
		// - le nom sans extension est construit à partir des éléments fournis sur le conteneur et
		//   de la configuration du nom pour le plugin.
		$fichier_cache = ezcache_cache_composer($plugin, $cache, $configuration);
	} elseif (is_string($cache)) {
		// Le chemin complet du fichier cache est fourni. Aucune vérification ne peut être faite
		// il faut donc que l'appelant ait utilisé l'API pour calculer le fichier au préalable.
		$fichier_cache = $cache;
	}

	// Détermination du nom du cache en fonction du plugin appelant et du type
	if ($fichier_cache) {
		// Lecture du fichier cache sécurisé ou pas suivant la configuration.
		include_spip('inc/flock');
		$lire = 'lire_fichier';
		if ($configuration['securisation']) {
			$lire = 'lire_fichier_securise';
		}
		$contenu_cache = '';
		$lecture_ok = $lire($fichier_cache, $contenu_cache);

		if ($lecture_ok) {
			if ($configuration['serialisation']) {
				$cache_lu = unserialize($contenu_cache);
			} elseif ($configuration['codage']) {
				// Etant donné qu'en CSV le codage précise le délimiteur et peut donc être différent de l'extension qui
				// permis lors de la configuration de définir le codage par défaut, on autorise le fait de pouvoir le
				// préciser dans le tableau canonique du cache.
				$codage = $cache['codage'] ?? '';
				$cache_lu = ezcache_cache_decoder($plugin, $codage, $contenu_cache, $configuration);
			} else {
				$cache_lu = $contenu_cache;
			}
		}
	}

	return $cache_lu;
}

/**
 * Teste l'existence d'un cache sur le disque spécifié par son identifiant relatif ou par son chemin complet et,
 * si il existe, teste si la date d'expiration du fichier n'est pas dépassée. Si le fichier existe et n'est pas périmé,
 * la fonction renvoie le chemin complet, sinon elle renvoie une chaine vide.
 *
 * @api
 *
 * @uses configuration_cache_lire()
 * @uses ezcache_cache_composer()
 * @uses ezcache_cache_valider()
 *
 * @param string                      $plugin     Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                                                ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param string                      $type_cache Type de cache identifiant la configuration dans la liste des configurations de cache du plugin
 * @param array<string, mixed>|string $cache      Identifiant du cache sous la forme d'une chaine (le chemin du fichier) ou d'un tableau fournissant
 *                                                les composants canoniques du nom.
 *
 * @return string Le chemin complet du fichier si valide, la chaine vide sinon.
 */
function cache_est_valide(string $plugin, string $type_cache, $cache) : string {
	// Lecture de la configuration des caches du plugin.
	$configuration = configuration_cache_lire($plugin, $type_cache);

	// Le cache peut-être fourni soit sous la forme d'un chemin complet soit sous la forme d'un
	// tableau permettant de calculer le chemin complet. On prend en compte ces deux cas.
	$fichier_cache = '';
	include_spip('ezcache/ezcache');
	if (
		is_array($cache)
		and ($cache = ezcache_cache_verifier($plugin, $cache, $configuration))
	) {
		// Détermination du chemin du cache :
		// - le nom sans extension est construit à partir des éléments fournis sur le conteneur et
		//   de la configuration du nom pour le plugin.
		$fichier_cache = ezcache_cache_composer($plugin, $cache, $configuration);
	} elseif (is_string($cache)) {
		// Le chemin complet du fichier cache est fourni. Aucune vérification ne peut être faite
		// il faut donc que l'appelant ait utilisé l'API cache_existe() pour calculer le fichier au préalable.
		$fichier_cache = $cache;
		// Dans ce cas, il faut retrouver le cache canonique sous forme tabulaire
		$cache = ezcache_cache_decomposer($plugin, $fichier_cache, $configuration);
	}

	// Vérifier maintenant la validité du cache.
	if (
		$fichier_cache
		and !ezcache_cache_valider($plugin, $fichier_cache, $cache, $configuration)
	) {
		$fichier_cache = '';
	}

	return $fichier_cache;
}

/**
 * Renvoie le chemin complet du cache sans tester son existence.
 * Cette fonction est une encapsulation du service ezcache_cache_composer().
 *
 * @api
 *
 * @uses configuration_cache_lire()
 * @uses ezcache_cache_verifier()
 * @uses ezcache_cache_composer()
 *
 * @param string               $plugin     Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                                         ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param string               $type_cache Type de cache identifiant la configuration dans la liste des configurations de cache du plugin
 * @param array<string, mixed> $cache      Identifiant du cache sous la forme d'un tableau fournissant les composants canoniques du nom.
 *
 * @return string Chemin complet du fichier cache
 */
function cache_nommer(string $plugin, string $type_cache, array $cache) : string {
	// Lecture de la configuration des caches du plugin.
	$configuration = configuration_cache_lire($plugin, $type_cache);

	// Le cache est toujours fourni sous la forme d'un tableau permettant de calculer le chemin complet.
	$fichier_cache = '';
	include_spip('ezcache/ezcache');
	if (
		is_array($cache)
		and ($cache = ezcache_cache_verifier($plugin, $cache, $configuration))
	) {
		// Détermination du chemin du cache :
		// - le nom sans extension est construit à partir des éléments fournis sur le conteneur et
		//   de la configuration du nom pour le plugin.
		$fichier_cache = ezcache_cache_composer($plugin, $cache, $configuration);
	}

	return $fichier_cache;
}

/**
 * Supprime le cache spécifié par son identifiant relatif ou par son chemin complet.
 *
 * @api
 *
 * @uses configuration_cache_lire()
 * @uses ezcache_cache_verifier()
 * @uses ezcache_cache_composer()
 * @uses ezcache_cache_decomposer()
 * @uses supprimer_fichier()
 *
 * @param string                      $plugin     Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                                                ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param string                      $type_cache Type de cache identifiant la configuration dans la liste des configurations de cache du plugin
 * @param array<string, mixed>|string $cache      Identifiant du cache sous la forme d'une chaine (le chemin du fichier) ou d'un tableau fournissant
 *                                                les composants canoniques du nom.
 * @param null|bool                   $post_cache `true` si on doit appeler le pipeline post_cache ou `false` sinon. Le défaut vaut `true`.
 *                                                Permet d'utiliser l'API dans le pipeline post_cache.
 *
 * @return bool `true` si la suppression s'est bien passée, `false` sinon.
 */
function cache_supprimer(string $plugin, string $type_cache, $cache, ?bool $post_cache = true) : bool {
	// Initialisation du contenu du cache
	$cache_supprime = false;

	// Lecture de la configuration des caches du plugin.
	$configuration = configuration_cache_lire($plugin, $type_cache);

	// Le cache peut-être fourni soit sous la forme d'un chemin complet soit sous la forme d'un
	// tableau permettant de calculer le chemin complet. On prend en compte ces deux cas.
	$fichier_cache = '';
	include_spip('ezcache/ezcache');
	if (
		is_array($cache)
		and ($cache = ezcache_cache_verifier($plugin, $cache, $configuration))
	) {
		// Détermination du chemin du cache :
		// - le nom sans extension est construit à partir des éléments fournis sur le conteneur et
		//   de la configuration du nom pour le plugin.
		$fichier_cache = ezcache_cache_composer($plugin, $cache, $configuration);
	} elseif (is_string($cache)) {
		// Le chemin complet du fichier cache est fourni. Aucune vérification ne peut être faite
		// il faut donc que l'appelant ait utilisé l'API cache_existe() pour calculer le fichier au préalable.
		$fichier_cache = $cache;
		$cache = ezcache_cache_decomposer($plugin, $fichier_cache, $configuration);
	}

	// Détermination du nom du cache en fonction du plugin appelant et du type
	if ($fichier_cache) {
		// Suppression du fichier.
		include_spip('inc/flock');
		$cache_supprime = supprimer_fichier($fichier_cache);

		// Appel d'un pipeline post opération sur le cache (écriture, suppression).
		// Ce pipeline peut être utilisé pour déloger le cache dans une liste par exemple.
		if ($post_cache) {
			$flux = [
				'args' => [
					'plugin'        => $plugin,
					'fonction'      => 'supprimer',
					'fichier_cache' => $fichier_cache,
					'cache'         => $cache,
					'configuration' => $configuration
				],
			];
			pipeline('post_cache', $flux);
		}
	}

	return $cache_supprime;
}

/**
 * Retourne la description des caches d'un plugin utilisateur filtrée sur un ensemble de critères. La description
 * de base fournie par Cache Factory contient les éléments de l’identifiant relatif mais peut-être remplacée ou
 * complétée par le plugin appelant au travers de services propres.
 * Les filtres concernent uniquement les éléments de l’identifiant relatif.
 *
 * @api
 *
 * @uses configuration_cache_lire()
 * @uses ezcache_cache_decomposer()
 * @uses ezcache_cache_completer()
 *
 * @param string                    $plugin     Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                                              ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param string                    $type_cache Type de cache identifiant la configuration dans la liste des configurations de cache du plugin
 * @param null|array<string, mixed> $filtres    Tableau associatif `[champ] = valeur` ou `[champ] = !valeur` de critères de filtres sur les composants
 *                                              de caches. Les opérateurs égalité et inégalité sont possibles.
 *
 * @return array Tableau des descriptions des fichiers cache créés par le plugin indexé par le chemin complet de
 *               chaque fichier cache.
 */
function cache_repertorier(string $plugin, string $type_cache, ?array $filtres = []) : array {
	// Initialisation de la liste des caches
	$caches = [];

	// Lecture de la configuration des caches du plugin.
	$configuration = configuration_cache_lire($plugin, $type_cache);

	// Rechercher les caches du plugin sans appliquer de filtre si ce n'est sur le sous-dossier, le préfixe éventuel
	// ou l'extension.
	// Les autres filtres seront appliqués sur les fichiers récupérés.
	$pattern_fichier = constant($configuration['racine']) . $configuration['dossier_plugin'];
	if ($configuration['sous_dossier']) {
		if (array_key_exists('sous_dossier', $filtres)) {
			$pattern_fichier .= rtrim($filtres['sous_dossier'], '/') . '/';
			unset($filtres['sous_dossier']);
		} elseif ($configuration['sous_dossier_auto']) {
			$pattern_fichier .= $type_cache . '/';
		} else {
			$pattern_fichier .= '*/';
		}
	}
	if ($configuration['nom_prefixe']) {
		$pattern_fichier .= $configuration['nom_prefixe'] . $configuration['separateur'];
		// Si un filtre existe avec le préfixe, on le supprime : le préfixe est connu sous l'index _nom_prefixe
		if (isset($filtres['_nom_prefixe'])) {
			unset($filtres['_nom_prefixe']);
		}
	}

	// On complète le pattern avec une recherche d'un nom quelconque mais avec l'extension configurée ou celle
	// du filtre.
	$extension = array_key_exists('extension', $filtres)
		? $filtres['extension']
		: $configuration['extension'];
	$pattern_fichier .= "*{$extension}";

	// On recherche les fichiers correspondant au pattern.
	$fichiers_cache = glob($pattern_fichier);

	if ($fichiers_cache) {
		foreach ($fichiers_cache as $_fichier_cache) {
			// On décompose le chemin de chaque cache afin de renvoyer l'identifiant canonique du cache.
			include_spip('ezcache/ezcache');
			$cache = ezcache_cache_decomposer($plugin, $_fichier_cache, $configuration);

			// Maintenant que les composants sont déterminés on applique les filtres pour savoir si on
			// complète et stocke le cache.
			$cache_conforme = true;
			foreach ($filtres as $_critere => $_valeur) {
				$operateur_egalite = true;
				$valeur = $_valeur;
				if (substr($_valeur, 0, 1) == '!') {
					$operateur_egalite = false;
					$valeur = ltrim($_valeur, '!');
				}
				if (
					!isset($cache[$_critere])
					or (
						isset($cache[$_critere])
						and (($operateur_egalite and ($cache[$_critere] != $valeur))
							or (!$operateur_egalite and ($cache[$_critere] == $valeur))))
				) {
					$cache_conforme = false;
					break;
				}
			}

			if ($cache_conforme) {
				// On permet au plugin de completer la description canonique
				$cache = ezcache_cache_completer($plugin, $cache, $_fichier_cache, $configuration);

				// On stocke la description du fichier cache dans le tableau de sortie.
				$caches[$_fichier_cache] = $cache;
			}
		}
	}

	return $caches;
}

/**
 * Supprime, pour un plugin donné, les caches désignés par leur chemin complet.
 *
 * @api
 *
 * @uses configuration_cache_lire()
 * @uses supprimer_fichier()
 * @uses ezcache_cache_decomposer()
 *
 * @param string    $plugin     Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                              ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param string    $type_cache Type de cache identifiant la configuration dans la liste des configurations de cache du plugin
 * @param array     $caches     Liste des fichiers caches désignés par leur chemin complet.
 * @param null|bool $post_cache `true` si on doit appeler le pipeline post_cache ou `false` sinon. Le défaut vaut `true`.
 *                              Permet d'utiliser l'API dans le pipeline post_cache.
 *
 * @return bool `true` si la suppression s'est bien passée, `false` sinon.
 */
function cache_vider(string $plugin, string $type_cache, array $caches, ?bool $post_cache = true) : bool {
	// Initialisation du retour
	$cache_vide = false;

	if ($caches) {
		// Lecture de la configuration des caches du plugin.
		$configuration = configuration_cache_lire($plugin, $type_cache);

		// Suppression des caches
		include_spip('ezcache/ezcache');
		include_spip('inc/flock');
		foreach ($caches as $_fichier) {
			supprimer_fichier($_fichier);

			// Appel d'un pipeline post opération sur le cache (écriture, suppression).
			// Ce pipeline peut être utilisé pour déloger le cache dans une liste par exemple.
			if ($post_cache) {
				$flux = [
					'args' => [
						'plugin'        => $plugin,
						'fonction'      => 'supprimer',
						'fichier_cache' => $_fichier,
						'cache'         => ezcache_cache_decomposer($plugin, $_fichier, $configuration),
						'configuration' => $configuration
					],
				];
				pipeline('post_cache', $flux);
			}
		}
		$cache_vide = true;
	}

	return $cache_vide;
}

/**
 * Recharge les configurations standard des types de cache d'un plugin utilisateur ou de tous les plugins utilisateur
 * ayant enregistrés une configuration.
 *
 * @api
 *
 * @uses ezcache_cache_configurer()
 *
 * @param null|string $plugin Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                            ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 *                            Si vide, toutes les configurations sont rechargées.
 *
 * @return void
 */
function configuration_cache_recharger(?string $plugin = '') : void {
	// Apple direct du service idoine
	include_spip('ezcache/ezcache');
	ezcache_cache_configurer($plugin);
}

/**
 * Lit la configuration standard d'un type de cache d'un plugin utilisateur, de tous les types de caches ou de tous
 * les plugins utilisateur ayant enregistrés une configuration.
 *
 * @api
 *
 * @uses lire_config()
 * @uses ezcache_cache_configurer()
 *
 * @param null|string $plugin     Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                                ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 *                                Si vide, toutes les configurations sont fournies.
 * @param null|string $type_cache Type de cache identifiant la configuration dans la liste des configurations de cache du plugin
 *                                Si vide toutes les configurations du plugin sont renvoyées.
 *
 * @return array<string, mixed> Tableau de configuration des caches d'un plugin utilisateur ou tableau vide si aucune configuration n'est encore
 *                              enregistrée.
 */
function configuration_cache_lire(?string $plugin = '', ?string $type_cache = '') : array {
	static $configuration = [];

	// Retourner la configuration d'un type de cache d'un plugin, de tous les types de cache d'un plugin ou
	// de tous les plugins utilisateur.
	include_spip('inc/config');
	if ($plugin) {
		// Lecture de la configuration des types de cache du plugin. Si celle-ci n'existe pas encore elle est créée.
		// On recharge toujours l'ensemble des types de cache d'un plugin
		if (
			empty($configuration[$plugin])
			and (!$configuration[$plugin] = lire_config("cache/{$plugin}", []))
		) {
			include_spip('ezcache/ezcache');
			$configuration[$plugin] = ezcache_cache_configurer($plugin);
		}

		// Suivant que la demande concerne un type de cache ou tous les types de caches du plugin
		if ($type_cache) {
			$configuration_lue = $configuration[$plugin][$type_cache] ?? [];
		} else {
			$configuration_lue = $configuration[$plugin];
		}
	} else {
		$configuration_lue = lire_config('cache', []);
	}

	return $configuration_lue;
}

/**
 * Efface les configurations standard de types de cache d'un plugin utilisateur ou de tous les plugins utilisateur
 * ayant enregistrés une configuration.
 *
 * @api
 *
 * @uses lire_config()
 * @uses effacer_config()
 *
 * @param null|string $plugin Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                            ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 *                            Si vide, toutes les configurations sont effacées.
 *
 * @return bool `true` si la suppression s'est bien passée, `false` sinon.
 */
function configuration_cache_effacer(?string $plugin = '') : bool {
	// Initialisation de la configuration à retourner
	include_spip('inc/config');
	$configuration_effacee = true;

	if ($plugin) {
		// Récupération de la meta du plugin Cache
		$configuration_plugin = lire_config("cache/{$plugin}", []);
		if ($configuration_plugin) {
			effacer_config("cache/{$plugin}");
		} else {
			$configuration_effacee = false;
		}
	} else {
		effacer_config('cache');
	}

	return $configuration_effacee;
}

/**
 * Lit la configuration du délimiteur CSV correspondant au codage (`csv`, `ssv`, ou `tsv`).
 *
 * @api
 *
 * @return array<string, mixed> Tableau de configuration du codage CSV qui définit le délimiteur.
 */
function configuration_codage_csv_lire() : array {
	include_spip('ezcache/ezcache');

	return _EZCACHE_CODAGE_CSV_DELIMITEUR;
}
