<?php
/**
 * Gestion du formulaire générique de vidage des caches d'un plugin donné utilisant Cache Factory.
 *
 * @package    SPIP\CACHE\UI
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données : le formulaire affiche la liste des caches administrables issus de l'utilisation du service et propose
 * le vidage de tout ou partie des fichiers.
 *
 * @uses configuration_cache_lire()
 * @uses cache_repertorier()
 * @uses ezcache_chercher_service()
 *
 * @param string     $plugin  Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                            ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param null|array $options Tableau d'options qui peut être fourni par un plugin utilisateur uniquement si celui-ci fait appel
 *                            au formulaire. La page cache_vider de Cache Factory n'utilise pas ce paramètre.
 *                            Le tableau est passé à la fonction de service de chargement du formulaire uniquement.
 *
 * @return array<string, mixed> Tableau des données à charger par le formulaire (affichage). Aucune donnée chargée n'est un
 *                              champ de saisie, celle-ci sont systématiquement remises à zéro. Le tableau comprend à minima l'index suivant:
 *                              - `_caches`    : (affichage) liste des descriptions des caches du plugin rangés par groupes
 */
function formulaires_cache_vider_charger(string $plugin, ?array $options = []) : array {
	// Stocker le préfixe et le nom du plugin de façon systématique.
	$valeurs = ['_prefixe_plugin' => $plugin];
	$informer = chercher_filtre('info_plugin');
	$valeurs['_nom_plugin'] = $informer($plugin, 'nom', true);

	// Récupérer les configurations de tous les types de cache du plugin et stocker la liste des types de cache
	// afin de les fournir en hidden au formulaire.
	include_spip('inc/ezcache_cache');
	$configurations = configuration_cache_lire($plugin);

	// On boucle sur chaque type de cache pour répertorier les caches existant
	$valeurs['_caches'] = [];
	$valeurs['_types_cache'] = [];
	$valeurs['_plugin_administrable'] = false;
	foreach ($configurations as $_type_cache => $_configuration) {
		// On ne collectionne que les caches administrables
		if ($_configuration['administration']) {
			// On collectionne les types de cache administrables et on modifie l'indicateur global
			$valeurs['_types_cache'][] = $_type_cache;
			$valeurs['_plugin_administrable'] = true;

			// On répertorie les caches pour vérifier qu'ils existent. Si non, on n'appelle aucune fonction spécifique.
			$caches = cache_repertorier($plugin, $_type_cache, []);

			// Le plugin utilisateur peut fournir un service propre pour construire le tableau des valeurs du formulaire
			// pour le type de cache concerné. Les valeurs doivent suivre la structure standard pour l'affichage.
			if ($caches) {
				if ($charger = ezcache_chercher_service($plugin, $_type_cache, 'cache_formulaire_charger')) {
					// Le plugin utilisateur doit suivre la structure minimale suivante :
					// -- ['_caches'][$_type_cache][$groupe] = array('titre', 'liste', 'explication')
					// -- ['_explications'][$_type_cache] = texte de l'explication
					$valeurs = $charger($plugin, $valeurs, $options, $_configuration);
				} else {
					// On présente simplement les fichiers caches en ordre alphabétique en visualisant uniquement
					// le sous-dossuer éventuel et le nom du fichier sans décomposition.
					// On construit un pseudo groupe unique sans titre dont l'id est le préfixe du plugin, ce qui permet de gérer
					// automatiquement les regroupements spécifiques de caches si besoin (par exemple, par services météo pour
					// Rainette).
					$valeurs['_caches'][$_type_cache][$plugin] = [
						'titre'       => '',
						'explication' => '',
						'liste'       => $caches
					];
				}
			}
		}
	}

	// On ne passe que les types de cache administrables
	$valeurs['_types_cache_hidden'] = implode(':', $valeurs['_types_cache']);

	return $valeurs;
}

/**
 * Vérification des saisies : il est obligatoire de choisir un cache à supprimer.
 *
 * @param string     $plugin  Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                            ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param null|array $options Tableau d'options qui peut être fourni par un plugin utilisateur uniquement si celui-ci fait appel
 *                            au formulaire. La page cache_vider de Cache Factory n'utilise pas ce paramètre.
 *                            Le tableau est passé à la fonction de service de chargement du formulaire uniquement.
 *
 * @return array<string, mixed> Tableau des erreurs qui se limite à la non sélection d'au moins un cache.
 */
function formulaires_cache_vider_verifier(string $plugin, ?array $options = []) : array {
	$erreurs = [];

	$types_cache = explode(':', _request('types_cache'));
	if ($types_cache !== false) {
		$caches = [];
		foreach ($types_cache as $_type_cache) {
			$caches_type = _request("caches_{$_type_cache}");
			if ($caches_type) {
				$caches = array_merge($caches, $caches_type);
			}
		}

		if (!$caches) {
			$erreurs['message_erreur'] = _T('cache:cache_vider_erreur_nocache');
		}
	}

	return $erreurs;
}

/**
 * Exécution du formulaire : la liste des caches sélectionnés est récupérée et fournie à l'API cache pour suppression.
 *
 * @uses cache_vider()
 *
 * @param string     $plugin  Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier
 *                            ou un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param null|array $options Tableau d'options qui peut être fourni par un plugin utilisateur uniquement si celui-ci fait appel
 *                            au formulaire. La page cache_vider de Cache Factory n'utilise pas ce paramètre.
 *                            Le tableau est passé à la fonction de service de chargement du formulaire uniquement.
 *
 * @return array<string, mixed> Tableau retourné par le formulaire contenant toujours un message de bonne exécution. L'indicateur
 *                              editable est toujours à vrai.
 *
 * @uses cache_vider()
 */
function formulaires_cache_vider_traiter(string $plugin, ?array $options = []) : array {
	$retour = [];

	// On récupère les caches à supprimer
	$types_cache = explode(':', _request('types_cache'));
	if ($types_cache === false) {
		$types_cache = [];
	}

	// On boucle sur tous les types pour acquérir la liste des caches à supprimer
	include_spip('inc/ezcache_cache');
	foreach ($types_cache as $_type_cache) {
		// On lit la liste des caches du type concerné
		$caches = _request("caches_{$_type_cache}");

		// On supprimer les caches sélectionnés du type concerné
		if ($caches) {
			cache_vider($plugin, $_type_cache, $caches);
		}
	}

	$retour['message_ok'] = _T('cache:cache_vider_succes');
	$retour['editable'] = true;

	return $retour;
}
