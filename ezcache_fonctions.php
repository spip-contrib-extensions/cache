<?php
/**
 * Ce fichier contient les balises de Cache Factory.
 *
 * @package SPIP\CACHE\BALISE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Compile la balise `#CACHE_LISTE` qui fournit la liste des caches pour un plugin utilisateur donné
 * et conformes aux filtres éventuellement fournis. Pour se focaliser sur un type de cache donné du plugin
 * il faut le fournir en filtre avec pour critère `type_cache`.
 * La signature de la balise est : `#CACHE_LISTE{plugin[, filtres]}`.
 *
 * @balise
 *
 * @uses traiter_balise_cache_liste()
 *
 * @example
 *     ```
 *     #CACHE_LISTE{ezcheck}, renvoie tous les caches du plugin ezcheck
 *     #CACHE_LISTE{ezcheck, #ARRAY{type_cache, dashboard}}, renvoie tous les caches du type 'dashboard' du plugin ezcheck
 *     #CACHE_LISTE{ezcheck, #ARRAY{type_cache, dashboard, objet, repo}}, renvoie les caches de type dashboard du plugin ezcheck dont l'objet est 'repo'
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 */
function balise_CACHE_LISTE_dist(Champ $p) : Champ {
	// Récupération des arguments.
	// -- le plugin est toujours nécessaire
	$plugin = interprete_argument_balise(1, $p);
	$plugin = isset($plugin) ? str_replace('\'', '"', $plugin) : '""';
	// -- les filtres sont optionnels
	$filtres = interprete_argument_balise(2, $p);
	$filtres = isset($filtres) ? str_replace('\'', '"', $filtres) : 'array()';

	// Appel de la fonction de listage (cache_repertorier).
	$p->code = "traiter_balise_cache_liste({$plugin}, {$filtres})";

	return $p;
}

/**
 * Renvoie la liste des caches.
 * Cette fonction encapsule l'API `cache_repertorier`.
 *
 * @internal
 *
 * @uses configuration_cache_lire()
 * @uses cache_repertorier()
 *
 * @param string     $plugin  Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                            un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 * @param null|array $filtres Tableau associatif `[champ] = valeur` ou `[champ] = !valeur` de critères de filtres sur les composants
 *                            de caches. Les opérateurs égalité et inégalité sont possibles.
 *
 * @return array
 */
function traiter_balise_cache_liste(string $plugin, ?array $filtres = []) {
	// Initialisation de la sortie
	$caches = [];

	// Déterminer si on veut les caches d'un type de cache ou de tous les types de caches du plugin
	include_spip('inc/ezcheck_cache');
	if (isset($filtres['type_cache'])) {
		// Recherche d'un filtre sur le type de cache
		$types_cache = [$filtres['type_cache']];
		unset($filtres['type_cache']);
	} else {
		$configurations = configuration_cache_lire($plugin);
		$types_cache = array_keys($configurations);
	}

	foreach ($types_cache as $_type_cache) {
		$caches = array_merge($caches, cache_repertorier($plugin, $_type_cache, $filtres));
	}

	return $caches;
}

/**
 * Compile la balise `#EZCACHE_PLUGINS` qui fournit les plugins utilisateur actifs implémentant des caches.
 * La signature de la balise est : `#EZCACHE_PLUGINS{[actifs_seuls]}`.
 *
 * @balise
 *
 * @example
 *     ```
 *     #EZCACHE_PLUGINS, renvoie la liste des préfixes de plugins actifs utilisant Cache Factory.
 *     #EZCACHE_PLUGINS{oui}, renvoie la liste des préfixes de plugins actifs utilisant Cache Factory.
 *     #EZCACHE_PLUGINS{non}, renvoie la liste des préfixes de tous plugins utilisant Cache Factory.
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_EZCACHE_PLUGINS_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Indicateur plugins actifs seulement ou tous
	$actifs_seuls = interprete_argument_balise(1, $p);
	$actifs_seuls = isset($actifs_seuls) ? str_replace('\'', '"', $actifs_seuls) : '"oui"';

	// Calcul de la balise
	$p->code = "traiter_balise_ezcache_plugins({$actifs_seuls})";

	return $p;
}

/**
 * Renvoie la liste des préfixes des plugins actifs ou tous les plugins utilisant Cache Factory.
 * L'utilisation du plugin Cache Factory est repérée via la meta `cache` dans la table `spip_meta`.
 *
 * @return array Liste des préfixes en minuscules
 */
function traiter_balise_ezcache_plugins($actifs_seuls = 'oui') {
	static $plugins = [];

	// Eviter les valeurs erronées en rendant l'indicateur binarie
	$exclure_inactif = ($actifs_seuls === 'oui');

	if (!isset($plugins[$exclure_inactif])) {
		$prefixes = [];

		// On cherche les plugins utilisateurs à partir des métas de consignation
		include_spip('inc/config');
		$config_cache = configuration_cache_lire();
		if ($config_cache) {
			$plugins = array_keys($config_cache);
			foreach ($plugins as $_plugin) {
				$prefixe = strtolower($_plugin);
				if (
					(
						$exclure_inactif
						and defined('_DIR_PLUGIN_' . strtoupper($prefixe))
					)
					or !$exclure_inactif
				) {
					$prefixes[] = $prefixe;
				}
			}
		}

		// Enregistrement de la liste
		$plugins[$exclure_inactif] = $prefixes;
	}

	return $plugins[$exclure_inactif];
}
