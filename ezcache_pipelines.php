<?php
/**
 * Ce fichier contient les cas d'utilisation de certains pipelines par le plugin Cache Factory.
 *
 * @package    SPIP\CACHE\PIPELINE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Supprime la meta stockant la configuration des caches de tous les plugins utilisateur quand
 * la page d'administration des plugins est affiché.
 *
 * C'est un peu grossier mais il n'existe pas de pipeline pour agir à la mise à jour d'un plugin.
 * Au moins, cela permet facilement de recharger une configuration d'un plugin utilisateur qui aurait changée
 * sans être une opération trop récurrente.
 *
 * @uses configuration_cache_lire()
 * @uses configuration_cache_effacer()
 * @uses ezcache_cache_configurer()
 *
 * @param array $flux Tableau des données permettant de caractériser la page concernée et de déclencher le traitement uniquement
 *                    sur la page `admin_plugin`.
 *
 * @return array Le flux entrant n'est pas modifié.
 */
function ezcache_affiche_milieu(array $flux) : array {
	if (isset($flux['args']['exec'])) {
		// Initialisation de la page du privé
		$exec = $flux['args']['exec'];

		if ($exec == 'admin_plugin') {
			// Administration des plugins

			// Supprime la meta du plugin Cache Factory de façon à mettre à jour la configuration des
			// plugins utilisateur si besoin.
			// Recharge la configuration des plugins utilisateur :
			// -- on ne lit pas la meta pour obtenir la liste des plugins mais on passe tous les plugins actifs
			//    ce qui permet à un nouveau plugin actif d'avoir sa configuration des caches à jour dès son activation
			include_spip('plugins/installer');
			if ($plugins = liste_plugin_actifs()) {
				// -- on supprime la meta
				include_spip('inc/ezcache_cache');
				configuration_cache_effacer();
				// -- on reconfigure chaque plugin en vérifiant que la configuration est bien à conserver
				foreach (array_keys($plugins) as $_plugin) {
					$plugin = strtolower($_plugin);
					if (
						strtolower($_plugin) !== 'ezcache'
						and defined('_DIR_PLUGIN_' . strtoupper($plugin))
						and is_dir(constant('_DIR_PLUGIN_' . strtoupper($plugin)) . '/ezcache')
					) {
						configuration_cache_recharger($plugin);
					}
				}
			}
		}
	}

	return $flux;
}
